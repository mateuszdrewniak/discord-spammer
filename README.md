<img src="assets/images/discord_spam.png" width="250" height="250">

# Discord Spammer

A small discord spammer app written in Ruby with Capybara.
![Demo GIF](assets/images/demo.gif)

## Prerequisites

- Ruby 2.7+
- Capybara
- ChromeDriver
- Chrome Web Browser

## Installing Ruby

### Windows

The most simple way to get Ruby running on Windows is to download and install the RubyInstaller executable [RubyInstaller executable](https://rubyinstaller.org/).

### MacOS

First, you should make sure that you've got [Brew](https://brew.sh/) installed
Run thse commands in the `Terminal`:

```console
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

Then you can just install Ruby through [Brew](https://brew.sh/)

```console
$ brew install ruby
```

## Installing ChromeDriver

### Windows

Here's a [Tutorial](https://youtu.be/dz59GsdvUF8) for installing ChromeDriver on Windows. Unfortunately it is a little bit harder than on MacOS.

### MacOS

Then you can just install ChromeDriver through [Brew](https://brew.sh/)

```console
$ brew cask install chromedriver
```

## Installing

1. Make sure that Ruby, Chrome and ChromeDriver are all installed on your system
2. Download this repo (or clone it)
3. Edit the `config.yml` file inside the repo you've downloaded
4. Enter your details in the `user` and `user_options` sections of this file
5. You can now run this bot by running the `start.bat` file if you're on Windows and `start.sh` when you're on MacOS/Linux
