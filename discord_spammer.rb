# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'

# Gem initialization

puts 'Installing and loading gems...'

Bundler.require(:default)
config = YAML.load_file('config.yml')
Bundler.require(config['environment'].to_s)

puts 'Gems installed and loaded!'

# Gem initialization

Capybara.default_driver = :selenium_chrome
Capybara.app_host = config['target']['host']
Capybara.run_server = false

Dir[File.join(__dir__, 'app', '**', '*.rb')].sort.each do |file|
  require file
end

puts 'Running the script...'
puts

begin
  Service::Spammer.new(config: config).spam!
rescue ::ErrorWithScreenshot => e
  e.page.save_screenshot('error_screenshot.png')
  puts
  puts "#{e.class} - error!"
  puts 'Fix your configuration'
  puts "Screenshot for debugging purposes has been saved as: #{__dir__}/error_screenshot.png"
rescue Selenium::WebDriver::Error::NoSuchWindowError, Selenium::WebDriver::Error::WebDriverError
  puts 'Window has been closed'
end

puts 'Terminating...'
