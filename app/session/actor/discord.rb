# frozen_string_literal: true

class Session::Actor::Discord < Session::Actor
  attr_accessor :textarea

  def setup!
    login!
    navigate_to_target!
  end

  def login!
    visit '/login'
    sleep 1
    find(:css, 'input[name="email"]').set(email)
    find(:css, 'input[name="password"]').set(password)
    find(:css, 'button[type="submit"]').click

    sleep 5
    verify_if_logged_in!
  rescue Capybara::ElementNotFound
    visit '/login'
    sleep 5
    verify_if_logged_in!
  end

  def navigate_to_target!
    server_links = find_all(:css, 'div[aria-label*="' + options['server_name'] + '"]')
    raise NoSuchSeverError.new(page: page) unless server_links

    server_links.first.click

    room_links = find_all(:css, 'div[aria-label*="' + options['room_name'] + '"]')
    raise NoSuchChatRoomError.new(page: page) unless room_links

    room_links.first.click

    activate_textarea!

    sleep 2
  end

  def type_and_send(text:)
    textarea.set(text)
    textarea.native.send_keys(:return)
  end

  private

  def verify_if_logged_in!
    raise CantLogInError.new(page: page) if find_all(:css, 'div svg div[role="listitem"]').size.zero?
  end

  def activate_textarea!
    @textarea = find(:css, 'div[class*="channelTextArea"] div[class*="slateTextArea"]')
    type_and_send(text: 'new')
  rescue Capybara::ElementNotFound
    raise CantFindChatTextareaError.new(page: page)
  end
end
