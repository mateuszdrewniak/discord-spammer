# frozen_string_literal: true

require 'capybara/dsl'

class Session::Actor
  class CantLogInError < ::ErrorWithScreenshot; end
  class NoSuchSeverError < ::ErrorWithScreenshot; end
  class NoSuchChatRoomError < ::ErrorWithScreenshot; end
  class CantFindChatTextareaError < ::ErrorWithScreenshot; end

  include Capybara::DSL

  attr_reader :email, :password, :driver, :options

  def initialize(email:, password:, driver: :selenium_chrome, options: {})
    @email = email
    @password = password
    @driver = driver.to_sym
    @options = options
  end

  def login!
    raise NotImplementedError
  end
end
