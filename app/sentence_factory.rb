# frozen_string_literal: true

require 'faker'

class SentenceFactory
  include Singleton

  def word
    WordFactory.instance
  end

  def call
    number = rand(1..12)

    case number
    when 1
      person_doing_something
    when 2
      person_has_never
    when 3
      noun_has_been
    when 4
      hacker_sentence
    when 5
      industry_status
    when 6
      shakespeare_quote
    when 7
      word.food(:description)
    when 8
      employee_status
    when 9
      company_description
    when 10
      bartender_beer
    when 11
      word.restaurant(:description)
    when 12
      word.restaurant(:review)
    end
  end

  def person_doing_something
    "#{word.name} is #{word.verb(:ing_form)} right now."
  end

  def person_has_never
    "#{word.name} has never #{word.verb(:past_participle)} anything."
  end

  def noun_has_been
    "The #{word.noun} has been #{word.verb(:past_participle)}."
  end

  def company_description
    case rand(1..2)
    when 1
      description_verb = rand(1..2).odd? ? 'operating' : 'specializing'
      "#{word.company} is a #{word.company(:buzzword)} company #{description_verb} in #{word.company(:industry)}. It is a #{word.company(:type)} firm."
    when 2
      separator = rand(1..2).odd? ? ',' : '-'
      "#{word.company} #{separator} #{word.company(:bs)}."
    end
  end

  def employee_status
    case rand(1..4)
    when 1
      "#{word.name} is the #{word.job} at #{word.company} and is doing #{word.adverb}"
    when 2
      ending = rand(1..2).odd? ? 'it seems to have been a good decision.' : "it's been a total disaster so far..."
      "#{word.name} is the #{word.job} at #{word.company} and #{ending}"
    when 3
      ending = rand(1..2).odd? ? 'no complaints whatsoever.' : 'many complaints...'
      "#{word.name} is the #{word.job} at #{word.company}. I've got #{ending}"
    when 4
      "#{word.name} is the #{word.job} at #{word.company}. Working with him is #{word.adjective}#{word.filler_ending}"
    end
  end

  def industry_status
    industry = word.company(:industry)
    be = industry.end_with?('s') ? 'are' : 'is'

    case rand(1..5)
    when 1
      "#{industry} #{be} booming right now!"
    when 2
      "#{industry} #{be} experiencing major growth."
    when 3
      "#{industry} #{be} bound to end up dominating the job market"
    when 4
      "#{industry} will create new jobs for #{word.number_range_name} of people."
    when 5
      "#{industry} #{be} contributing to a lot of Earth's polution."
    end
  end

  def bartender_beer
    encourage = rand(1..2).odd? ? encourage_to_try : ''
    "#{word.beer(:name)} - #{word.beer(:alcohol)} alcohol with #{word.beer(:malts)}, #{word.beer(:ibu)} and #{word.beer(:blg)}. #{encourage}"
  end

  def encourage_to_try
    case rand(1..7)
    when 1
      'Would you like to try it?'
    when 2
      'Do you want some?'
    when 3
      "Give it a try, you won't regret it!"
    when 4
      "May I offer it to you?"
    when 5
      "It will do you good on this rough day."
    when 6
      "One of our most desired beverages!"
    when 7
      "It's selling #{word.adverb}."
    end
  end

  def hacker_sentence
    Faker::Hacker.say_something_smart
  end

  def shakespeare_quote
    case rand(1..2)
    when 1
      Faker::Quotes::Shakespeare.as_you_like_it_quote
    when 2
      Faker::Quotes::Shakespeare.king_richard_iii_quote
    end
  end
end
