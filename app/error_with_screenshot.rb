# frozen_string_literal: true

class ErrorWithScreenshot < StandardError
  attr_accessor :page

  def initialize(message: nil, page: nil)
    super(message)
    @page = page
  end
end
