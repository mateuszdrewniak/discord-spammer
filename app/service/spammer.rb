# frozen_string_literal: true

class Service::Spammer
  class UnsupportedSiteError < ::ErrorWithScreenshot; end

  attr_reader :config, :actor, :sentence_factory

  def initialize(config:)
    @config = config
    @actor = dispatch_actor!.new(email: config.dig('user', 'email'),
                                 password: config.dig('user', 'password'),
                                 driver: config['driver'],
                                 options: config['user_options'])
    @sentence_factory = SentenceFactory.instance
  end

  def spam!
    actor.setup!
    start_sending
  end

  private

  def start_sending
    loop do
      actor.type_and_send(text: sentence_factory.call)
      sleep 1
    end
  end

  def dispatch_actor!
    Session::Actor.const_get(config['target']['site'])
  rescue NameError
    raise UnsupportedSiteError.new(page: page)
  end
end
