# frozen_string_literal: true

require 'faker'

class WordFactory
  include Singleton

  def name(type = :name)
    Faker::Name.public_send(type)
  end

  def animal
    Faker::Creature::Animal.name
  end

  def noun(without_name: true)
    number = rand(1..9)
    case number
    when 1
      without_name ? university : name
    when 2
      animal
    when 3
      rand(1..2).odd? ? beer : beer(:name)
    when 4
      programming_language
    when 5
      color
    when 6
      vehicle
    when 7
      device_company
    when 8
      device
    when 9
      food
    end
  end

  def adverb
    case rand(1..7)
    when 1
      'pretty well'
    when 2
      'quite well'
    when 3
      'horribly'
    when 4
      'reasonably well'
    when 5
      'awfully badly'
    when 6
      'absolutely'
    when 7
      'all right'
    end
  end

  def adjective
    case rand(1..7)
    when 1
      'challenging'
    when 2
      'amazing'
    when 3
      'refreshing'
    when 4
      'crazy'
    when 5
      'unique'
    when 6
      'mesmerising'
    when 7
      'abhorrent'
    end
  end

  def filler_ending
    case rand(1..8)
    when 1
      ', so to speak.'
    when 2
      ', as it were.'
    when 3
      ' honestly.'
    when 4
      ' to my great surprise.'
    when 5
      ', in a way.'
    when 6
      '!'
    when 7
      ', sort of.'
    when 8
      ', so to say.'
    end
  end

  def verb(type = :base)
    Faker::Verb.public_send(type)
  end

  def number_range_name
    case rand(1..6)
    when 1
      'hundreds'
    when 2
      'thousands'
    when 3
      'tens of thousands'
    when 4
      'houndreds of thousands'
    when 5
      'millions'
    when 6
      'billions'
    end
  end

  def food(type = :dish)
    Faker::Food.public_send(type)
  end

  def color
    Faker::Color.color_name
  end

  def beer(type = :brand)
    Faker::Beer.public_send(type)
  end

  def programming_language
    Faker::ProgrammingLanguage.name
  end

  def currency(type = :name)
    Faker::Currency.public_send(type)
  end

  def university(type = :name)
    Faker::University.public_send(type)
  end

  def vehicle(type = :make_and_model)
    Faker::Vehicle.public_send(type)
  end

  def device_company
    Faker::Device.manufacturer
  end

  def device(type = :model_name)
    Faker::Device.public_send(type)
  end

  def company(type = :name)
    Faker::Company.public_send(type)
  end

  def job(type = :title)
    Faker::Job.public_send(type)
  end

  def restaurant(type = :name)
    Faker::Restaurant.public_send(type)
  end
end
